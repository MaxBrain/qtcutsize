#ifndef TASK_H
#define TASK_H

#include <vector>
#include "detail.h"
#include "rect.h"
#include "placedrect.h"
#include "dimension.h"

struct Point
{
    int x{0};
    int y{0};
};

class Task
{
private:
    bool inList(int x, int y) const;
    bool canPlace(int x, int y, int width, int height) const;
    bool in(int p, int from, int to) const;
    bool intersected(int x00, int y00, int x01, int y01, int x10, int y10, int x11, int y11) const;
    std::vector<Point> angles;
    int calcSpending() const;
    void addRect(const Rect& rect);
private:
    QString message;
    std::vector<PlacedRect> result;
    Dimension spending;
public:
    std::vector<Detail> details;
    Rect list;
    void resolve();
    QString getMessage() const;
    std::vector<PlacedRect> getResult() const;
    Dimension getSpending() const;
};

#endif // TASK_H
