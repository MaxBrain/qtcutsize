#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "task.h"
#include "detailsdrawer.h"
#include "schemadrawer.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

    void update_width();
    void update_height();
    int current_row{0};
    bool update_details();
    void messageBox(const QString& message);

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    DetailsDrawer* detailsDrawer;
    SchemaDrawer* schemaDrawer;

private slots:
    void on_button_calculate_clicked();

    void on_button_add_detail_clicked();

    void on_button_remove_detail_clicked();

    void on_edit_width_editingFinished();

    void on_edit_height_editingFinished();

    void on_table_details_cellChanged(int row, int column);

    void on_table_details_cellActivated(int row, int column);

    void on_table_details_cellClicked(int row, int column);

    void on_table_details_cellEntered(int row, int column);

    void on_table_details_cellPressed(int row, int column);

private:
    Ui::MainWindow *ui;
    Task task;
};
#endif // MAINWINDOW_H
