#include "schemadrawer.h"
#include <QPainter>

SchemaDrawer::SchemaDrawer(const Task& task, QWidget *parent)
    : QWidget(parent)
    , task(task)
{
    resize(parent->size());
}
void SchemaDrawer::paintEvent(QPaintEvent *)
{
    QPainter p;
    p.begin(this);
    float scale = 0.03f;
    p.drawRect(0, 0, task.list.lenght.getValue() * scale, task.list.width.getValue() * scale);
    for (const auto& placedDetail : task.getResult()) {
        p.drawRect(placedDetail.x.getValue() * scale, placedDetail.y.getValue() * scale,
                   placedDetail.size.lenght.getValue() * scale, placedDetail.size.width.getValue() * scale);
    }
    p.end();
}
