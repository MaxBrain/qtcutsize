#include "intvalue.h"

void IntValue::set(const QString& string)
{
    QString tmp{string};
    tmp.replace(',', '.');
    bool ok;
    int result = tmp.toDouble(&ok);
    if (!ok) return;
    if (result<=0) return;
    value = result;
}

void IntValue::set(int val)
{
    if (val>0) value = val;
}

QString IntValue::getString() const
{
    return QString::number(value);
}

int IntValue::getValue() const
{
    return value;
}
