QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    detail.cpp \
    detailsdrawer.cpp \
    dimension.cpp \
    intvalue.cpp \
    main.cpp \
    mainwindow.cpp \
    placedrect.cpp \
    rect.cpp \
    schemadrawer.cpp \
    task.cpp

HEADERS += \
    detail.h \
    detailsdrawer.h \
    dimension.h \
    intvalue.h \
    mainwindow.h \
    placedrect.h \
    rect.h \
    schemadrawer.h \
    task.h

FORMS += \
    mainwindow.ui

LIBS += -L"C:/Soft/Qt/Tools/mingw810_64/x86_64-w64-mingw32/lib"

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
