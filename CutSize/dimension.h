#ifndef DIMENSION_H
#define DIMENSION_H

#include <QString>

class Dimension
{
    int value{0};
public:
    void set(const QString& string);
    void set(int val);
    QString getString() const;
    int getValue() const;
};

#endif // DIMENSION_H
