#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    detailsDrawer = new DetailsDrawer(task, ui->widget_details_image);
    detailsDrawer->show();
    schemaDrawer = new SchemaDrawer(task, ui->widget_schema);
    schemaDrawer->show();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::update_width() {
    task.list.width.set(ui->edit_width->text());
    ui->edit_width->setText(task.list.width.getString());
}

void MainWindow::update_height() {
    task.list.lenght.set(ui->edit_height->text());
    ui->edit_height->setText(task.list.lenght.getString());
}

void MainWindow::on_button_calculate_clicked()
{
    update_width();
    update_height();
    if (update_details()) {
        task.resolve();
        messageBox(task.getMessage());
        ui->value_consumption->setText(task.getSpending().getString()+" погонных сантиметров");
    }
    else
    {
        messageBox("Не все параметры деталей заполнены");
    }
}

bool MainWindow::update_details()
{
    task.details.resize(ui->table_details->rowCount());
    for (size_t row=0;row<task.details.size();row++) {
        if (ui->table_details->item(row,0)!=nullptr) {
            task.details[row].count.set(ui->table_details->item(row,0)->text());
            ui->table_details->item(row,0)->setText(task.details[row].count.getString());
        }
        else
        {
            return false;
        }
        if (ui->table_details->item(row,1)!=nullptr) {
            task.details[row].size.width.set(ui->table_details->item(row,1)->text());
            ui->table_details->item(row,1)->setText(task.details[row].size.width.getString());
        }
        else
        {
            return false;
        }
        if (ui->table_details->item(row,2)!=nullptr) {
            task.details[row].size.lenght.set(ui->table_details->item(row,2)->text());
            ui->table_details->item(row,2)->setText(task.details[row].size.lenght.getString());
        }
        else
        {
            return false;
        }
    }
    return true;
}


void MainWindow::messageBox(const QString& message)
{
    QMessageBox msgBox;
    msgBox.setText(message);
    msgBox.exec();
}

void MainWindow::on_button_add_detail_clicked()
{
    ui->table_details->insertRow(ui->table_details->rowCount());
}

void MainWindow::on_button_remove_detail_clicked()
{
    ui->table_details->removeRow(current_row);
}

void MainWindow::on_edit_width_editingFinished()
{
    update_width();
}

void MainWindow::on_edit_height_editingFinished()
{
    update_height();
}

void MainWindow::on_table_details_cellChanged(int, int)
{
    update_details();
}

void MainWindow::on_table_details_cellActivated(int row, int)
{
    current_row = row;
}

void MainWindow::on_table_details_cellClicked(int row, int)
{
    current_row = row;
}

void MainWindow::on_table_details_cellEntered(int row, int)
{
    current_row = row;
}

void MainWindow::on_table_details_cellPressed(int row, int)
{
    current_row = row;
}
