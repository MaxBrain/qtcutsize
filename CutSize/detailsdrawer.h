#ifndef DETAILSDRAWER_H
#define DETAILSDRAWER_H

#include <QWidget>
#include "task.h"

class DetailsDrawer : public QWidget
{
    Q_OBJECT
private:
    const Task& task;
public:
    explicit DetailsDrawer(const Task& task, QWidget *parent = nullptr);
protected:
    void paintEvent(QPaintEvent *) override;
};

#endif // DETAILSDRAWER_H
