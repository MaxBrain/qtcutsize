#ifndef INTVALUE_H
#define INTVALUE_H

#include <QString>

class IntValue
{
    int value{0};
public:
    void set(const QString& string);
    void set(int val);
    QString getString() const;
    int getValue() const;
};

#endif // INTVALUE_H
