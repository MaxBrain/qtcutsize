#include "dimension.h"

void Dimension::set(const QString& string)
{
    QString tmp{string};
    tmp.replace(',', '.');
    bool ok;
    int result = tmp.toDouble(&ok)*10;
    if (!ok) return;
    if (result<=0) return;
    value = result;
}

void Dimension::set(int val)
{
    if (val>=0) value = val;
}

QString Dimension::getString() const
{
    return QString::number(static_cast<float>(value)/10, 'f', 1);
}

int Dimension::getValue() const
{
    return value;
}
