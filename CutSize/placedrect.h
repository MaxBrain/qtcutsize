#ifndef PLACEDRECT_H
#define PLACEDRECT_H

#include "rect.h"
#include "dimension.h"

class PlacedRect
{
public:
    Dimension x;
    Dimension y;
    Rect size;
};

#endif // PLACEDRECT_H
