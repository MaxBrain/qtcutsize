#ifndef SCHEMADRAWER_H
#define SCHEMADRAWER_H

#include <QWidget>
#include "task.h"

class SchemaDrawer : public QWidget
{
    Q_OBJECT
private:
    const Task& task;
public:
    explicit SchemaDrawer(const Task& task, QWidget *parent = nullptr);
protected:
    void paintEvent(QPaintEvent *) override;
};

#endif // SCHEMADRAWER_H
