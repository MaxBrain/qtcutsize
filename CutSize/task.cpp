#include "task.h"
#include <iostream>
#include <QList>
#include <QRect>

bool Task::intersected(int x00, int y00, int x01, int y01, int x10, int y10, int x11, int y11) const
{
    if (x00>=x11) return false;
    if (x01<=x10) return false;
    if (y00>=y11) return false;
    if (y01<=y10) return false;
    return true;
}

int Task::calcSpending() const
{
    int len = 0;
    for (const auto& rect : result) {
        len = std::max(len, rect.x.getValue()+rect.size.lenght.getValue());
    }
    return len;
}

bool Task::inList(int x, int y) const
{
    if (x<0) return false;
    if (y<0) return false;
    if (x>list.lenght.getValue()) return false;
    if (y>list.width.getValue()) return false;
    return true;
}

bool Task::canPlace(int x, int y, int lenght, int width) const
{
    int x00 = x;
    int y00 = y;
    int x01 = x + lenght;
    int y01 = y + width;
    if (!inList(x00,y00)) return false;
    if (!inList(x01,y01)) return false;
    for (const auto& rect : result) {
        int x10 = rect.x.getValue();
        int y10 = rect.y.getValue();
        int x11 = x10 + rect.size.lenght.getValue();
        int y11 = y10 + rect.size.width.getValue();
        if (intersected(x00, y00, x01, y01, x10, y10, x11, y11)) return false;
    }
    return true;
}

void Task::addRect(const Rect& rect)
{
    bool found = false;
    size_t angle_id;
    bool rotated;
    int selected_spending;
    int selected_x;

    for (size_t i = 0; i<angles.size(); i++)
    {
        const auto& angle = angles[i];
        if (canPlace(angle.x, angle.y, rect.lenght.getValue(), rect.width.getValue()))
        {
            PlacedRect radd;
            radd.x.set(angle.x);
            radd.y.set(angle.y);
            radd.size.lenght.set(rect.lenght.getValue());
            radd.size.width.set(rect.width.getValue());
            result.push_back(radd);
            int curr_spending = calcSpending();
            result.pop_back();
            int curr_x = angle.x;
            if (
                !found ||
                (curr_spending<selected_spending) ||
                ((curr_spending==selected_spending)&&(curr_x<selected_x))
            ) {
                found = true;
                angle_id = i;
                rotated = false;
                selected_spending = curr_spending;
                selected_x = curr_x;
            }
        }
        if (canPlace(angle.x, angle.y, rect.width.getValue(), rect.lenght.getValue()))
        {
            PlacedRect radd;
            radd.x.set(angle.x);
            radd.y.set(angle.y);
            radd.size.lenght.set(rect.width.getValue());
            radd.size.width.set(rect.lenght.getValue());
            result.push_back(radd);
            int curr_spending = calcSpending();
            result.pop_back();
            int curr_x = angle.x;
            if (
                !found ||
                (curr_spending<selected_spending) ||
                ((curr_spending==selected_spending)&&(curr_x<selected_x))
            ) {
                found = true;
                angle_id = i;
                rotated = true;
                selected_spending = curr_spending;
                selected_x = curr_x;
            }
        }
    }
    if (found) {
        const auto& angle = angles[angle_id];
        PlacedRect radd;
        radd.x.set(angle.x);
        radd.y.set(angle.y);
        radd.size.lenght.set(rotated?rect.width.getValue():rect.lenght.getValue());
        radd.size.width.set(rotated?rect.lenght.getValue():rect.width.getValue());
        result.push_back(radd);
        Point angle1;
        angle1.x = angle.x + radd.size.lenght.getValue();
        angle1.y = angle.y;
        Point angle2;
        angle2.x = angle.x;
        angle2.y = angle.y + radd.size.width.getValue();
        angles.erase(angles.begin()+angle_id);
        angles.push_back(angle1);
        angles.push_back(angle2);
    }
    else
    {
        message+="Невозможно расположить деталь размером "+rect.lenght.getString()+"x"+rect.width.getString()+"\n";
    }
}

void Task::resolve()
{
    result.clear();
    message.clear();
    auto work_details{details};

    std::sort(work_details.begin(), work_details.end(),
    [](const Detail& l, const Detail& r)->bool {
        return l.area()<r.area();
    });
    angles.clear();
    angles.push_back(Point{0,0});
    while (!work_details.empty())
    {
        addRect(work_details.back().size);
        if (work_details.back().count.getValue() == 1) {
            work_details.pop_back();
        } else {
            work_details.back().count.set(work_details.back().count.getValue()-1);
        }
    }
    spending.set(calcSpending());
    if (message.isEmpty()) message = "Детали удачно расположены на листе";
}

QString Task::getMessage() const
{
    return message;
}

std::vector<PlacedRect> Task::getResult() const
{
    return result;
}

Dimension Task::getSpending() const
{
    return spending;
}
