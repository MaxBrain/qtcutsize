#ifndef DETAIL_H
#define DETAIL_H

#include "rect.h"
#include "intvalue.h"

class Detail
{
public:
    IntValue count;
    Rect size;
    int area() const;
};

#endif // DETAIL_H
