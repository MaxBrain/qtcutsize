#include "detailsdrawer.h"
#include <QPainter>

DetailsDrawer::DetailsDrawer(const Task& task, QWidget *parent)
    : QWidget(parent)
    , task(task)
{
    resize(parent->size());
}

void DetailsDrawer::paintEvent(QPaintEvent *)
{
    QPainter p;
    p.begin(this);
    float scale = 0.03f;
    float x = 0;
    const float distance = 200;
    for (const auto& placedDetail : task.getResult()) {
        p.drawRect(x * scale, 0, placedDetail.size.lenght.getValue() * scale, placedDetail.size.width.getValue() * scale);
        x = x + placedDetail.size.lenght.getValue() + distance;
    }
    p.end();
}
