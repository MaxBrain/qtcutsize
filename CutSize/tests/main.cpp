#define _CRT_SECURE_NO_DEPRECATE
#include <QApplication>
#include <QTest>
#include <cstdlib>
#include <cstdio>
#include "testdetail.h"
#include "testdimension.h"
#include "testintvalue.h"
#include "testplacedrect.h"
#include "testrect.h"
#include "testtask.h"

int main(int argc, char *argv[])
{
    freopen("testing.log", "w", stdout);
    QApplication a(argc, argv);
    QTest::qExec(new TestDetail, argc, argv);
    QTest::qExec(new TestDimension, argc, argv);
    QTest::qExec(new TestIntValue, argc, argv);
    QTest::qExec(new TestPlacedRect, argc, argv);
    QTest::qExec(new TestRect, argc, argv);
    QTest::qExec(new TestTask, argc, argv);
    return 0;
}
