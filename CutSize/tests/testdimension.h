#ifndef TESTDIMENSION_H
#define TESTDIMENSION_H

#include <QObject>

class TestDimension : public QObject
{
    Q_OBJECT

private slots:
    void test();
};

#endif // TESTDIMENSION_H
