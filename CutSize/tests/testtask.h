#ifndef TESTTASK_H
#define TESTTASK_H

#include <QObject>

class TestTask : public QObject
{
    Q_OBJECT

private slots:
    void test();
};

#endif // TESTTASK_H
