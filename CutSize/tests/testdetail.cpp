#include <QtTest>
#include <type_traits>
#include "testdetail.h"
#include "detail.h"
#include "intvalue.h"


void TestDetail::test()
{
    Detail detail;
    QTEST_ASSERT((std::is_same<decltype(detail.count),  IntValue>::value));
    QTEST_ASSERT((std::is_same<decltype(detail.size),  Rect>::value));
    QCOMPARE(detail.area(), 0);
    detail.size.lenght.set(3);
    QCOMPARE(detail.area(), 0);
    detail.size.width.set(4);
    QCOMPARE(detail.area(), 12);
    detail.size.lenght.set("3");
    detail.size.width.set("4");
    QCOMPARE(detail.area(), 1200);
    QCOMPARE(detail.count.getValue(), 0);
    detail.count.set(2);
    QCOMPARE(detail.count.getValue(), 2);
}
