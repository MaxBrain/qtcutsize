#include <QtTest>
#include "testrect.h"
#include "rect.h"

void TestRect::test()
{
    Rect rect;
    QCOMPARE(rect.lenght.getValue(), 0);
    QCOMPARE(rect.width.getValue(), 0);
    QCOMPARE(rect.lenght.getString(), "0.0");
    QCOMPARE(rect.width.getString(), "0.0");
    rect.lenght.set("2.0");
    rect.width.set("1.0");
    QCOMPARE(rect.lenght.getString(), "2.0");
    QCOMPARE(rect.width.getString(), "1.0");
    rect.lenght.set("-2.0");
    rect.width.set("-1.0");
    QCOMPARE(rect.lenght.getString(), "2.0");
    QCOMPARE(rect.width.getString(), "1.0");
    rect.lenght.set("3.43");
    rect.width.set("1.12");
    QCOMPARE(rect.lenght.getString(), "3.4");
    QCOMPARE(rect.width.getString(), "1.1");
    rect.lenght.set("0");
    rect.width.set("0");
    QCOMPARE(rect.lenght.getString(), "3.4");
    QCOMPARE(rect.width.getString(), "1.1");
}
