#include <QtTest>
#include "testdimension.h"
#include "dimension.h"

void TestDimension::test()
{
    Dimension dimension;
    QCOMPARE(dimension.getValue(), 0);
    QCOMPARE(dimension.getString(), "0.0");
    dimension.set("2.0");
    QCOMPARE(dimension.getString(), "2.0");
    dimension.set("-2.0");
    QCOMPARE(dimension.getString(), "2.0");
    dimension.set("3.43");
    QCOMPARE(dimension.getString(), "3.4");
    dimension.set("0");
    QCOMPARE(dimension.getString(), "3.4");
}
