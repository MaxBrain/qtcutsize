#ifndef TESTDETAIL_H
#define TESTDETAIL_H

#include <QObject>

class TestDetail : public QObject
{
    Q_OBJECT

private slots:
    void test();
};

#endif // TESTDETAIL_H
