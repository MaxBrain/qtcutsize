#include <QtTest>
#include "testintvalue.h"
#include "intvalue.h"

void TestIntValue::test()
{
    IntValue value;
    QCOMPARE(value.getValue(), 0);
    QCOMPARE(value.getString(), "0");
    value.set("2.0");
    QCOMPARE(value.getString(), "2");
    value.set("-2.0");
    QCOMPARE(value.getString(), "2");
    value.set("3.43");
    QCOMPARE(value.getString(), "3");
    value.set("0");
    QCOMPARE(value.getString(), "3");
    value.set("4");
    QCOMPARE(value.getString(), "4");
}
