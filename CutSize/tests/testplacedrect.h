#ifndef TESTPLACEDRECT_H
#define TESTPLACEDRECT_H

#include <QObject>

class TestPlacedRect : public QObject
{
    Q_OBJECT

private slots:
    void test();
};

#endif // TESTPLACEDRECT_H
