#include <QtTest>
#include "rect.h"

// add necessary includes here

class RectTest : public QObject
{
    Q_OBJECT

public:
    RectTest();
    ~RectTest();

private slots:
    void test_default_values();

};

RectTest::RectTest()
{

}

RectTest::~RectTest()
{

}

void RectTest::test_default_values()
{
    Rect rect;
    QCOMPARE(rect.getHeight(), 0);
    QCOMPARE(rect.getWidth(), 0);
}
