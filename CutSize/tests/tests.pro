QT += testlib
QT += gui
QT += widgets

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

INCLUDEPATH += ..

SOURCES +=  \
    ../detail.cpp \
    ../dimension.cpp \
    ../intvalue.cpp \
    ../placedrect.cpp \
    ../task.cpp \
    main.cpp \
    testdetail.cpp \
    testdimension.cpp \
    testintvalue.cpp \
    testplacedrect.cpp \
    testrect.cpp \
    ../rect.cpp \
    testtask.cpp

HEADERS += \
    testdetail.h \
    testdimension.h \
    testintvalue.h \
    testplacedrect.h \
    testrect.h \
    testtask.h
