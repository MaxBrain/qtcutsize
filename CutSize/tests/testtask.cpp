#include <QtTest>
#include "testtask.h"
#include "task.h"

void TestTask::test()
{
    Task task;
    Detail detail1;
    detail1.count.set(2);
    detail1.size.lenght.set("100");
    detail1.size.width.set("200");
    Detail detail2;
    detail2.count.set(3);
    detail2.size.lenght.set("70");
    detail2.size.width.set("70");
    task.details.clear();
    task.details.push_back(detail1);
    task.details.push_back(detail2);
    task.list.width.set("286");
    task.list.lenght.set("1500");
    task.resolve();
    QCOMPARE(task.getSpending().getString(), "210.0");
    QCOMPARE(task.getResult().size(), 5);
    QCOMPARE(task.getMessage(), "Детали удачно расположены на листе");
}
