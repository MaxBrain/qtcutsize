#ifndef TESTINTVALUE_H
#define TESTINTVALUE_H

#include <QObject>

class TestIntValue : public QObject
{
    Q_OBJECT

private slots:
    void test();
};

#endif // TESTINTVALUE_H
