#include <QtTest>
#include "testplacedrect.h"
#include "placedrect.h"

void TestPlacedRect::test()
{
    PlacedRect placedRect;
    Q_ASSERT((std::is_same<decltype(placedRect.x), Dimension>::value));
    Q_ASSERT((std::is_same<decltype(placedRect.y), Dimension>::value));
    Q_ASSERT((std::is_same<decltype(placedRect.size), Rect>::value));
}
