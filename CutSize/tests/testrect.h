#ifndef TESTRECT_H
#define TESTRECT_H

#include <QObject>

class TestRect : public QObject
{
    Q_OBJECT

private slots:
    void test();
};

#endif // TESTRECT_H
